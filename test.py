from mitmproxy import proxy, options
from mitmproxy.tools.dump import DumpMaster
from mitmproxy.addons import core
from mitmproxy import ctx

class AddHeader:
    def __init__(self):
        self.num = 0

    def response(self, flow):
        self.num = self.num + 1
        print(self.num)
        flow.response.headers["count"] = str(self.num)



class Counter:
    def __init__(self):
        self.num = 0

    def request(self, flow):
        self.num = self.num + 1
        ctx.log.info("We've seen %d flows" % self.num)

addons = [
    Counter() #AddHeader()
]

def main():
    opts = options.Options(listen_host = '127.0.0.1', listen_port = 8080)
    opts.add_option("body_size_limit", int, 0, "")
    opts.add_option("keep_host_header", bool, True, "")
    pconf = proxy.config.ProxyConfig(opts)
    m = DumpMaster(None)
    m.server = proxy.server.ProxyServer(pconf)
    print(m.addons)
    m.addons.add(addons)
    try:
        m.run()
    except:
        m.shutdown()

    return


main()
